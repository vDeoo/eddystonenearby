package com.invokemedia.nearbynotifier;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

/**
 * The only activity in this sample. Sets up a retained fragment to which provides most of the
 * functionality for this sample.
 */
public class MainActivity extends AppCompatActivity {
    private static final String MAIN_FRAGMENT_TAG = "main_fragment_tag";

    private MainActivityFragment mMainFragment;
    private static final int REQUEST_RESOLVE_ERROR = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fm = getFragmentManager();
        mMainFragment = (MainActivityFragment) fm.findFragmentByTag(MAIN_FRAGMENT_TAG);

        if (mMainFragment == null) {
            mMainFragment = new MainActivityFragment();
            fm.beginTransaction().add(R.id.container, mMainFragment, MAIN_FRAGMENT_TAG).commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mMainFragment.finishedResolvingNearbyPermissionError();
        if (requestCode == REQUEST_RESOLVE_ERROR) {
            // User was presented with the Nearby opt-in dialog and pressed "Allow".
            if (resultCode == Activity.RESULT_OK) {
                // We track the pending subscription tasks in MainFragment. Once
                // a user gives consent to use Nearby, we execute those tasks.
                mMainFragment.executePendingSubscriptionTask();
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // User was presented with the Nearby opt-in dialog and pressed "Deny". We cannot
                // proceed with any pending subscription tasks. Reset state.
                mMainFragment.resetToDefaultState();
            } else {
                Toast.makeText(this, "Failed to resolve error with code " + resultCode,
                        Toast.LENGTH_LONG).show();
            }
        }
    }
}
