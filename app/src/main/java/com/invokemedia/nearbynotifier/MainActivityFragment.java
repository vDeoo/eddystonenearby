package com.invokemedia.nearbynotifier;


import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.iid.InstanceID;
import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.MessageListener;
import com.google.android.gms.nearby.messages.NearbyMessagesStatusCodes;
import com.google.android.gms.nearby.messages.Strategy;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class MainActivityFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        SharedPreferences.OnSharedPreferenceChangeListener{

    private static final String TAG = "MainActivityFragment";

    private static final Strategy BEACON_STRATEGY = Strategy.BLE_ONLY;

    private static final int REQUEST_RESOLVE_ERROR = 1001;
    private static final int NO_LONGER_SUBSCRIBING = 1234;

    // The time-to-live when subscribing or publishing in this sample. Three minutes.
    private static final int TTL_IN_SECONDS = 3 * 60;
    private static final int TTL_IN_MILLISECONDS = TTL_IN_SECONDS * 1000;

    private static final String KEY_SUBSCRIPTION_TASK = "subscription_task";
    private static final String TASK_SUBSCRIBE = "task_subscribe";
    private static final String TASK_UNSUBSCRIBE = "task_unsubscribe";
    private static final String TASK_NONE = "task_none";


    // Views.
    private ProgressBar mSubscriptionProgressBar;
    private ImageButton mSubscriptionImageButton;

    /**
     * Adapter for working with messages from nearby devices.
     */
    private ArrayAdapter<String> mNearbyDevicesArrayAdapter;

    /**
     * Backing data structure for {@code mNearbyDevicesArrayAdapter}.
     */

    private final ArrayList<String> mNearbyDevicesArrayList = new ArrayList<>();

    /**
     * Provides an entry point for Google Play services.
     */
    private GoogleApiClient mGoogleApiClient;


    /**
     * A {@link MessageListener} for processing messages from nearby devices.
     */
    private MessageListener mMessageListener;


    /**
     * Tracks if we are currently resolving an error related to Nearby permissions. Used to avoid
     * duplicate Nearby permission dialogs if the user initiates both subscription and publication
     * actions without having opted into Nearby.
     */
    private boolean mResolvingNearbyPermissionError = false;

    /**
     * A Handler that resets the state when a subscription expires or when the device is no longer
     * publishing.
     */
    private ResetStateHandler mResetStateHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Use a retained fragment to avoid re-publishing or re-subscribing upon orientation
        // changes.
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        mSubscriptionProgressBar = (ProgressBar) view.findViewById(
                R.id.subscription_progress_bar);
        mSubscriptionImageButton = (ImageButton) view.findViewById(R.id.subscription_image_button);
        mSubscriptionImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String subscriptionTask = getSubscriptionTask(KEY_SUBSCRIPTION_TASK);
                if (TextUtils.equals(subscriptionTask, TASK_NONE) ||
                        TextUtils.equals(subscriptionTask, TASK_UNSUBSCRIBE)) {
                    updateSharedPreference(KEY_SUBSCRIPTION_TASK,
                            TASK_SUBSCRIBE);
                } else {
                    updateSharedPreference(KEY_SUBSCRIPTION_TASK,
                            TASK_UNSUBSCRIBE);
                }
            }
        });

        final ListView nearbyDevicesListView = (ListView) view.findViewById(
                R.id.nearby_devices_list_view);
        mNearbyDevicesArrayAdapter = new ArrayAdapter<>(getActivity().getApplicationContext(),
                android.R.layout.simple_list_item_1,
                mNearbyDevicesArrayList);
        nearbyDevicesListView.setAdapter(mNearbyDevicesArrayAdapter);
        mMessageListener = new MessageListener() {
            @Override
            public void onFound(final Message message) {
                Log.i(TAG, "Found message: " + DeviceMessage.fromNearbyMessage(message).getMessageBody());
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mNearbyDevicesArrayAdapter.add(
                                DeviceMessage.fromNearbyMessage(message).getMessageBody());
                    }
                });
            }

            @Override
            public void onLost(final Message message) {
                // Called when a message is no longer detectable nearby.
                Log.i(TAG, "Lost message: " + message);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mNearbyDevicesArrayAdapter.remove(
                                DeviceMessage.fromNearbyMessage(message).getMessageBody());
                    }
                });
            }
        };

        updateUI();
        return view;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart(){
        super.onStart();

        getActivity().getPreferences(Context.MODE_PRIVATE)
                .registerOnSharedPreferenceChangeListener(this);

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity().getApplicationContext())
                .addApi(Nearby.MESSAGES_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

    }

    @Override
    public void onStop() {
        if (mGoogleApiClient.isConnected() && !getActivity().isChangingConfigurations()) {
            unsubscribe();
            updateSharedPreference(KEY_SUBSCRIPTION_TASK, TASK_NONE);
            mGoogleApiClient.disconnect();
            getActivity().getPreferences(Context.MODE_PRIVATE)
                    .unregisterOnSharedPreferenceChangeListener(this);
        }
        super.onStop();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(TAG, "GoogleApiClient connected");
        // If the user has requested a subscription or publication task that requires
        // GoogleApiClient to be connected, we keep track of that task and execute it here, since
        // we now have a connected GoogleApiClient.
        executePendingSubscriptionTask();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // For simplicity, we don't handle connection failure thoroughly in this sample. Refer to
        // the following Google Play services doc for more details:
        // http://developer.android.com/google/auth/api-client.html
        Log.i(TAG, "connection to GoogleApiClient failed");
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i(TAG, "GoogleApiClient connection suspended: "
                + connectionSuspendedCauseToString(cause));
    }

    private static String connectionSuspendedCauseToString(int cause) {
        switch (cause) {
            case CAUSE_NETWORK_LOST:
                return "CAUSE_NETWORK_LOST";
            case CAUSE_SERVICE_DISCONNECTED:
                return "CAUSE_SERVICE_DISCONNECTED";
            default:
                return "CAUSE_UNKNOWN: " + cause;
        }
    }


    /**
     * Based on values stored in SharedPreferences, determines the subscription task
     * that should be performed.
     */
    private String getSubscriptionTask(String taskKey) {
        return getActivity()
                .getPreferences(Context.MODE_PRIVATE)
                .getString(taskKey, TASK_NONE);
    }

    /**
     * Invokes a pending task based on the subscription state.
     */
    void executePendingSubscriptionTask() {
        String pendingSubscriptionTask = getSubscriptionTask(KEY_SUBSCRIPTION_TASK);
        if (TextUtils.equals(pendingSubscriptionTask, TASK_SUBSCRIBE)) {
            subscribe();
        } else if (TextUtils.equals(pendingSubscriptionTask, TASK_UNSUBSCRIBE)) {
            unsubscribe();
        }
    }

    /**
     * Subscribes to messages from nearby devices
     */
    private void subscribe() {
        Log.i(TAG, "trying to subscribe");
        // Cannot proceed without a connected GoogleApiClient. Reconnect and execute the pending
        // task in onConnected().

        if (!mGoogleApiClient.isConnected()) {
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        } else {
            Nearby.Messages.subscribe(mGoogleApiClient, mMessageListener, BEACON_STRATEGY)
                    .setResultCallback(new ResultCallback<Status>() {

                        @Override
                        public void onResult(Status status) {
                            if (status.isSuccess()) {
                                Log.i(TAG, "subscribed successfully");
                                sendMessageToHandler(NO_LONGER_SUBSCRIBING,
                                        TTL_IN_MILLISECONDS);
                            } else {
                                Log.i(TAG, "could not subscribe");
                                handleUnsuccessfulNearbyResult(status);
                            }
                        }
                    });
        }
    }

    /**
     * Sends an {@link android.os.Message} to {@link ResetStateHandler} to change state when a
     * publication or a subscription end.
     *
     * @param messageID         Value to assign to the returned Message.what field.
     * @param ttlInMilliseconds Time to wait before enqueuing the Message.
     */
    void sendMessageToHandler(int messageID, int ttlInMilliseconds) {
        android.os.Message msg = mResetStateHandler.obtainMessage(messageID);
        mResetStateHandler.sendMessageDelayed(msg, ttlInMilliseconds);
    }

    private void unsubscribe() {
        Log.i(TAG, "trying to unsubscribe");
        // Cannot proceed without a connected GoogleApiClient. Reconnect and execute the pending
        // task in onConnected().
        if (!mGoogleApiClient.isConnected()) {
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        } else {
            Nearby.Messages.unsubscribe(mGoogleApiClient, mMessageListener)
                    .setResultCallback(new ResultCallback<Status>() {

                        @Override
                        public void onResult(Status status) {
                            if (status.isSuccess()) {
                                Log.i(TAG, "unsubscribed successfully");
                            } else {
                                Log.i(TAG, "could not unsubscribe");
                                handleUnsuccessfulNearbyResult(status);
                            }
                        }
                    });
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, final String key) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (TextUtils.equals(key, KEY_SUBSCRIPTION_TASK)) {
                    executePendingSubscriptionTask();
                    updateUI();
                }
            }});
    }

    /**
     * Updates the UI when the state of a subscription or publication action changes.
     */
    private void updateUI() {
        String subscriptionTask = getSubscriptionTask(KEY_SUBSCRIPTION_TASK);

        // Using Nearby is battery intensive. For this reason, progress bars are visible when
        // subscribing or publishing.
        mSubscriptionProgressBar.setVisibility(
                TextUtils.equals(subscriptionTask, TASK_SUBSCRIBE) ? View.VISIBLE :
                        View.INVISIBLE);
        mSubscriptionImageButton.setImageResource(
                TextUtils.equals(subscriptionTask, TASK_SUBSCRIBE) ?
                        R.drawable.ic_cancel : R.drawable.ic_nearby);
    }

    /**
     * Helper for editing entries in SharedPreferences.
     */
    private void updateSharedPreference(String key, String value) {
        getActivity().getPreferences(Context.MODE_PRIVATE)
                .edit()
                .putString(key, value)
                .apply();
    }

    public void resetToDefaultState(){
        getActivity().getPreferences(Context.MODE_PRIVATE)
                .edit()
                .putString(KEY_SUBSCRIPTION_TASK, TASK_NONE)
                .apply();
    }

    protected void finishedResolvingNearbyPermissionError() {
        mResolvingNearbyPermissionError = false;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mResetStateHandler = new ResetStateHandler(this);
    }

    /**
     * Handles errors generated when performing a subscription or publication action. Uses
     * {@link Status#startResolutionForResult} to display an opt-in dialog to handle the case
     * where a device is not opted into using Nearby.
     */
    private void handleUnsuccessfulNearbyResult(Status status) {
        Log.i(TAG, "processing error, status = " + status);
        if (status.getStatusCode() == NearbyMessagesStatusCodes.APP_NOT_OPTED_IN) {
            if (!mResolvingNearbyPermissionError) {
                try {
                    mResolvingNearbyPermissionError = true;
                    status.startResolutionForResult(getActivity(),
                            REQUEST_RESOLVE_ERROR);

                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
            }
        } else {
            if (status.getStatusCode() == ConnectionResult.NETWORK_ERROR) {
                Toast.makeText(getActivity().getApplicationContext(),
                        "No connectivity, cannot proceed. Fix in 'Settings' and try again.",
                        Toast.LENGTH_LONG).show();
                resetToDefaultState();
            } else {
                // To keep things simple, pop a toast for all other error messages.
                Toast.makeText(getActivity().getApplicationContext(), "Unsuccessful: " +
                        status.getStatusMessage(), Toast.LENGTH_LONG).show();
            }

        }
    }
    /**
     * Handler for updating state when a device is no longer publishing or subscribing.
     */
    private static class ResetStateHandler extends Handler {
        private final WeakReference<MainActivityFragment> mWeakReference;

        ResetStateHandler(MainActivityFragment mainActivity) {
            mWeakReference = new WeakReference<>(mainActivity);
        }

        @Override
        public void handleMessage(android.os.Message msg) {
            MainActivityFragment mainFragment = mWeakReference.get();
            switch (msg.what) {
                case NO_LONGER_SUBSCRIBING:
                    if (mainFragment != null) {
                        mainFragment.updateSharedPreference(KEY_SUBSCRIPTION_TASK,
                                TASK_NONE);
                    }
                    break;
            }
        }
    }

}
